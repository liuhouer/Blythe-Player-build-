1.8.5-------2012/10/28---------by bruce

1.调整界面布局，修改滚动信息的算法。
2.重新编译。删除冗余资源。减小cpu消耗.
3.修改歌词秀界面界面代码，减少bug。
4.优化隐藏播放列表切换歌曲的bug.
5.优化文件不存在时处理异常的交互信息.
6.优化id3v1处理tag标签相关功能。
7.此版本已极度稳定。

//----------------------------------------------------------
1. Adjust interface layout, modify the rolling information algorithm.
2. Recompile. Delete redundant resources. Reduced consumption of CPU.
3. Modify the lyrics show interface interface code, reduce the bug.
4. Optimize hidden playlist switch song bug.
5. Optimization file does not exist when handling abnormal mutual information.
6. Optimization id3v1 processing tag label related function.
