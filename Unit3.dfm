object LrcShow: TLrcShow
  Left = 557
  Top = 147
  Width = 301
  Height = 394
  Caption = #27468#35789#31168
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnMouseDown = FormMouseDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lst1: TListBox
    Left = 0
    Top = 0
    Width = 285
    Height = 356
    Align = alClient
    BevelInner = bvSpace
    BevelKind = bkFlat
    BevelOuter = bvSpace
    BorderStyle = bsNone
    Color = 10982031
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -16
    Font.Name = #24494#36719#38597#40657
    Font.Style = []
    ImeName = #20013#25991' ('#31616#20307') - '#35895#27468#25340#38899#36755#20837#27861
    ItemHeight = 21
    ParentFont = False
    PopupMenu = PopupMenu1
    TabOrder = 0
    OnDrawItem = lst1DrawItem
  end
  object tmr1: TTimer
    Enabled = False
    Interval = 500
    OnTimer = tmr1Timer
    Left = 152
    Top = 128
  end
  object OBFormMagnet1: TOBFormMagnet
    Active = True
    FormGlue = False
    MainFormMagnet = True
    Left = 152
    Top = 72
  end
  object PopupMenu1: TPopupMenu
    Left = 152
    Top = 32
    object N1: TMenuItem
      Caption = #26700#38754#27468#35789
      OnClick = N1Click
    end
    object N2: TMenuItem
      Caption = #21152#36733#27468#35789
      OnClick = N2Click
    end
    object N3: TMenuItem
      Caption = #25628#32034#27468#35789
      OnClick = N3Click
    end
    object N4: TMenuItem
      Caption = #32534#36753#27468#35789
      OnClick = N4Click
    end
  end
end
